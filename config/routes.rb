Rails.application.routes.draw do
  root to:'pages#home'
  get 'pages/home'

  get 'pages/health_check'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
