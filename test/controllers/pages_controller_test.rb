require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  test "should get home" do
    get pages_home_url
    assert_response :success
  end

  test "should get health_check" do
    get pages_health_check_url
    assert_response :success
  end

end
